/**
* Created by fujisho on 15/01/09.
*/

var express = require('express');
var router = express.Router();
var fs = require('fs');
var multiparty = require('multiparty');
var model = require('../models/mongoose.js');
var Icon = model.Icon;



router.get('/_test', function(req, res){
  res.send('<form action="/upload" enctype="multipart/form-data" method="post"><input type="file" name="uploadFile"><input type="submit"></form>');
});


/**
 * アイコンファイル名一覧取得
 */
router.get('/icon', function(req, res){
  var msg = '';

  Icon.find({}, function(err, data){
    if (err) {
      console.log('err');
    } else {
      var array = new Array();
      for (var i = 0; i < data.length; i++){
        array[i] = data[i].name;
      }
      msg = array.toString();
    }

    res.send(msg);
  });
});






router.post('/icon', function (req, res) {
  var form = new multiparty.Form();

  form.parse(req, function(err, fields, files){
    if (err){
      console.log(err);
      req.send("error");

    } else {
      //console.log(files);
      res.send('success');

      var name = files.image[0].originalFilename;
      var tmpPath = files.image[0].path;
      var savPath = '../public/images/icons/' + name;

      fs.rename(tmpPath, savPath, function (err) {
        if (err) {
          console.log('file is not moved: ', err);
          fs.unlink(tmpPath, function(err2) {
            if (err2) {
              console.log('tmp file is not removed: ', err2);
            }
            console.log('tmp file is removed');
          });
        } else {
          var newIcon = new Icon({name:name});
          newIcon.save(function(err){
            if (err) {
              console.log('icon data is not saved :', err);
            }
          });
        }
      });
    }

  });
});




//
//exports.post = function(req, res) {
//  var target_path, tmp_path;
//  console.dir(req.files);
//  tmp_path = req.files.thumbnail.path;
//  target_path = '../public/icons/' + req.files.thumbnail.originalname;
//  fs.rename(tmp_path, target_path, function(err) {
//    if (err) {
//      throw err;
//    }
//    fs.unlink(tmp_path, function() {
//      if (err) {
//        throw err;
//      }
//      res.send('File uploaded to: ' + target_path + ' - ' + req.files.thumbnail.size + ' bytes');
//    });
//  });
//};


module.exports = router;