var express = require('express');
var router = express.Router();
var model = require('../models/mongoose.js');
var User  = model.User;

/* GET users listing. */
router.get('/', function(req, res) {
  var u_name = req.query.name;

  if (u_name == null) {
    res.send('invalid query');
  } else {
    var query = { name: u_name };

    User.findOne(query, function(err, data) {
      if (err) {
        console.log(err);
      } else {
        if (data != null) {
          var id = data.id;
          res.redirect('/users/' + id);
        } else {
          res.send('登録されていないユーザです');
        }
      }

    });

  }

});

router.get('/:id', function(req, res) {
  //res.send('get ' + req.params.id);
  res.render('ws', {
    userId: req.params.id
  });
});

router.get('/:id/config', function(req, res){
  res.send('config api');

});



module.exports = router;