/**
* Created by fujisho on 15/01/05.
*/

var express = require('express');
var router  = express.Router();
var model = require('../models/mongoose.js');
var User  = model.User;


router.get('/', function(req, res) {
  res.render('register/index', {
    title: 'ユーザ登録',
    name: '',
    errors: {}
  });
});

// POSTでフォームから飛んできたデータをモデルに保存、失敗したらフォームに戻す
router.post('/confirm', function(req, res) {

  var newUser = new User(req.body);
  newUser.save(function(err){
    if (err) {
      console.log(err);
      res.render('register/index', {
        title: 'ユーザ登録エラー',
        errors: err.errors
      });
    } else {
      //req.session.u_name = req.body.u_name;
      res.redirect('/register/complete');
    }
  })
});

router.get('/complete', function(req, res) {
  //res.render('register/complete', {
  //  title: 'ユーザ登録完了'
  //});

  res.send('登録完了');
});




module.exports = router;