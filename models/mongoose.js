var DB_NAME = 'desktop';

var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var autoIncrement = require("mongoose-auto-increment"); // save()のみ有効
mongoose.connect('mongodb://localhost/'+DB_NAME);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
    console.log('connection success');
});
autoIncrement.initialize(db);

/**
 * user
 */
var userSchema = mongoose.Schema({
    name: { type: String, required: '名前を入力してください', unique: true}
    //email : { type: String, required: 'メールアドレスを入力してください', unique: true }
});

userSchema.plugin(uniqueValidator, { message: 'すでに登録されている名前です' });
userSchema.plugin(autoIncrement.plugin, 'User');
var User = mongoose.model('User', userSchema);

exports.User = User;


var fileSchema = mongoose.Schema({
    //id: {type:Number, unique:true},
    owner:Number,
    info: {
        name: String,
        icon: String,
        isdir: Boolean,
        isExt: Boolean,
        x: Number,
        y: Number
    }
});

fileSchema.plugin(uniqueValidator);
fileSchema.plugin(autoIncrement.plugin, 'File');
var File = mongoose.model('File', fileSchema);
exports.File = File;


var iconSchema = mongoose.Schema({
    //id: {type:Number, unique:true},
    name : {type:String, unique:true}
});

iconSchema.plugin(uniqueValidator);
iconSchema.plugin(autoIncrement.plugin, 'Icon');
var Icon = mongoose.model('Icon', iconSchema);
exports.Icon = Icon;