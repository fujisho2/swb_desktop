/**
 * Created by fujisho on 15/01/05.
 */

var socket_io = require('socket.io');
var model = require('../models/mongoose.js');
var Icon = model.Icon;
var User = model.User;
var File = model.File;



module.exports = sio;

function sio(server) {

  // Socket.IO
  var sio = socket_io.listen(server);
  var userId;


  // 接続
  sio.sockets.on('connection', function(socket) {


    //console.log('client connected sid = ' + sid);

    // user id
    socket.on('userId', function(data){
      userId = parseInt(data);
      User.findById(userId, function(err, res){
        if (err) {
          console.log(err);
        } else {
          if (res == null) {
            //TODO alert and redirect
            console.log('invalid user id');
          } else {
            File.find({'owner':userId}, function(err, docs){
              if (err){
                console.log(err);
              } else {
                var arr = new Array();
                for (var i = 0; i < docs.length; i++) {
                  arr[i] = docs[i].info;
                }
                socket.emit('initFileInfo', arr);
              }
            });
          }
        }
      })
    });



    /**
     * デスクトップ情報を受信
     * DB更新
     * data = [{}
     */
    socket.on('userDesktopInfo', function(data){
      //var json = JSON.parse(data);
      var userId = parseInt(data.owner);
      var fileInfo = data.info;

      if (fileInfo.name != null) {
        File.findOne({'owner': userId, 'info.name': fileInfo.name}, function(err, docs){
          if (err) {
            console.log(err);
          } else {
            // save or update
            if (docs == null) {
              var newFile = new File(data);
              newFile.save(function (err) {
                if (err) {
                  console.log(err);
                } else {

                }
              });
            } else {
              File.update(docs, {$set:data}, {upsert: true}, function(err, num, raw){
                if (err){
                  console.log(err);
                } else {

                }
              });

            }
          }
        });

      }
    });


    socket.on('changePosition', function(data){
      var x = data.pos.left;
      var y = data.pos.top;
      var name = data.name;

      File.update({'owner':userId, 'info.name':name},{$set:{'info.x':x,'info.y':y}}, {upsert:true} ,function(err, n, r){
        if (err){
          console.log(err);
        } else {

        }
      });
      socket.broadcast.emit('changePosition', data);

    });




    // 切断
    socket.on("disconnect", function() {
    });






  });
}


function initFileInfo(userId) {
  File.find({owner:{id:userId}}, function(err, data){
    if (err){
      console.log(err);
      return null;
    } else {
      var arr = new Array();
      for (var i = 0; i < data.length; i++) {
        arr[i] = data[i].info;
      }
      socket.emit('initFileInfo', arr);
    }
  });
}