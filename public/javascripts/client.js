/**
 * Created by fujisho on 15/01/04.
 */

var socket = io.connect('http://localhost:3000');

var userDesktopInfo;
var userId;

$(function() {
  userId = $('title').text();

  $('.icon').draggable();

  console.log('uid is ' + userId);


  socket.on('connect', function() {

    socket.emit('userId', userId);
    console.log('connected server');

  });

  socket.on('disconnect', function(client) {
    socket.disconnect();
  });


  socket.on('drawDesktop', function () {

  });

  socket.on('initFileInfo', function(data){
    drawDesktop(data);
  });


  //$('.icon').animate({
  //  'left': '500px'
  //},1000);
  //
  //$('.icon').draggable();

  /**
   * TODO: アイコン移動の同期 今はリロード
   */
  socket.on('changePosition', function(data){
    var x = data.pos.left;
    var y = data.pos.top;
    var name = data.name;

    location.reload();

  });



});


/**
 * ネイティブコードで取得したデスクトップ情報をサーバに送信する
 *
 * @param data :JSON
 */
function sendUserDesktopInfo(data) {

  drawDesktop(data);

  for (var i = 0; i < data.length; i++) {
    var fileInfo = {"owner": userId, "info": data[i]};
    //var fileInfo2 = JSON.stringify(fileInfo);
    socket.emit('userDesktopInfo', fileInfo);

  }



  //userDesktopInfo = {"owner": userId, "icons": JSON.parse(data)};

  //drawDesktop(userDesktopInfo.icons);
  //console.log(desktopData);

  //socket.emit('userDesktopInfo', userDesktopInfo);
}

/**
 * TODO: 座標のズレ修正
 * @param file
 */
function drawDesktop(file){
  var code = '';

  for (var i = 0; i < file.length; i++) {
    if (file[i].name != null) {
      var top  = file[i].y - 45;
      var left = file[i].x - 57;
      var img  = file[i].icon + '.png';
      if (img == '.png') {
        img = '___.png';
      }
      var name = file[i].name;
      code += '<p class="abs icon file_' + i + '" ';
      code += 'style="top:' + top;
      code += 'px;left:' + left + 'px">';
      code += '<img src="/images/icons/'+ img + '">';
      code += name;
      code += '</p>';
    }
  }
  $('#desktop').html(code);
  $('.icon').draggable({
    start: function() {
      //$(this).addClass("ui-state-hover").find("p").html("ドラッグ開始...");
    },
    stop: function() {
      var pos = $(this).position();
      var name = $(this).text();

      socket.emit('changePosition', {'pos':pos, 'name':name});
    }
  });
}

function changePosition(data) {


  //$

}